# Change to the project directory
PROJECT_FOLDER='folder_name'
PHP_VER=7.2
ENV=production

cd /var/www/html/${PROJECT_FOLDER} || true

# Turn on maintenance mode
php artisan down --message 'The app is being (quickly!) updated. Please try again in a minute.' || true

# Pull the latest changes from the git repository

git reset --hard
# git clean -df
git pull origin #git branch here

# Install/update composer dependecies

if [[ $ENV == 'production' ]]; then
  composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev
    echo "production environment"
else
    composer install
    echo "dev environment"
fi

# Restart FPM
#( flock -w 10 9 || exit 1
#    echo 'Restarting FPM...'; sudo -S service $FORGE_PHP_FPM reload ) 9>/tmp/fpmlock

#restart php
sudo service php${PHP_VER}-fpm restart
#Restart nginx
sudo nginx -s reload

# Run database migrations
php artisan migrate --force

# Clear caches
php artisan cache:clear

# Clear expired password reset tokens
php artisan auth:clear-resets

# Clear and cache routes
php artisan route:cache

# Clear and cache config
php artisan config:cache

# Clear and cache views
php artisan view:cache

# Restart queue broadcasting
php artisan queue:restart

# Install node modules
npm ci


# Build assets using Laravel Mix
if [[ $ENV == 'production' ]]; then
  npm run production
    echo "npm running in production"
else
    npm run dev
    echo "npm running in dev"
fi

# Turn off maintenance mode
php artisan up

echo 'Project Deployed'
